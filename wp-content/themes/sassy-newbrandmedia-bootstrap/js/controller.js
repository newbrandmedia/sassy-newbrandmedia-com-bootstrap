

angular.module('app').controller("MainController", function(){
    var vm = this;
	vm.title = 'AngularJS Tutorial Example';
});


angular.module('app').controller('MasterLayoutCtrl', ['$scope', '$http', '$rootScope', '$location',
  function ($scope, $http, $rootScope, $location) {
    $scope.pageTitle = 'master';
    $('.bxslider').bxSlider();
   //Redirect to the new location regardless of if it has anchor name
    $rootScope.linkTo = function(id) {
    $location.url(id);
   };

  }]);

angular.module('app').controller('cartVehicleCtrl', ['$scope', '$http', '$rootScope', '$location','$anchorScroll',
  function($scope, $http, $rootScope, $location, $anchorScroll) {
    $scope.pageTitle = 'About the Vehicle';
    $rootScope.showCart=true;
    $scope.dofocus=function(sectionName){
    $location.hash(sectionName);
    $anchorScroll();
    }
}]);

angular.module('app').controller('homeApCtrl', ['$scope', '$http', '$rootScope', '$location', '$anchorScroll',
    function($scope, $http, $rootScope, $location,  $anchorScroll) {
    $scope.pageTitle = 'Home';
    $rootScope.showCart=false;
    $('.bxslider').bxSlider();
    $scope.dofocus=function(sectionName){
    $location.hash(sectionName);
    $anchorScroll();
    }
}]); 

angular.module('app').controller('homeCtrl', ['$scope', '$http', '$rootScope', '$location', '$anchorScroll',
    function($scope, $http, $rootScope, $location,  $anchorScroll) {
    $scope.pageTitle = 'Home';
    $rootScope.showCart=false;
    $('.bxslider').bxSlider();
    $scope.dofocus=function(sectionName){
    $location.hash(sectionName);
    $anchorScroll();
    }
}]); 

angular.module('app').controller('cartRecall', ['$scope', '$http', '$rootScope', '$location', '$anchorScroll',
    function($scope, $http, $rootScope, $location,  $anchorScroll) {
      $scope.pageTitle = 'Recall your existing quote';
    $rootScope.showCart=false;
    $scope.dofocus=function(sectionName){
    $location.hash(sectionName);
    $anchorScroll();
    }
}]); 

angular.module('app').controller('home', ['$scope', '$http', '$rootScope', '$location',
  function ($scope, $http, $rootScope, $location) {
    $scope.$parent.pageTitle = 'Home Page';
	$scope.$parent.pageSubtitle = null;
	$scope.$parent.pageClass = '';
	
}]);

angular.module('app').controller('cartLogin', ['$scope', '$http', '$rootScope', '$location',
  function ($scope, $http, $rootScope, $location) {
    $scope.$parent.pageTitle = 'Login';
  $scope.$parent.pageSubtitle = null;
  $scope.$parent.pageClass = '';
  
}]);



angular.module('app').controller('cartDriver', ['$scope', '$http', '$rootScope', '$location',
  function($scope, $http, $rootScope, $location) {
    $scope.$parent.pageTitle = 'Driver Details';
  $scope.$parent.pageSubtitle = null;
  $scope.$parent.pageClass = '';
  
}]);
  

angular.module('app').controller('productGapCtrl', ['$scope', '$http', '$rootScope', '$location', '$anchorScroll',
  function($scope, $http, $rootScope, $location,  $anchorScroll) {
      $scope.pageTitle = 'Mercedes-Benz GAP Insurance';
      $rootScope.showCart=false;
      $scope.dofocus=function(sectionName){
    $location.hash(sectionName);
    $anchorScroll();
    }
}]);


angular.module('app').controller('productGapLeaseCtrl', ['$scope', '$http', '$rootScope', '$location',
  function($scope, $http, $rootScope, $location) {
      $scope.pageTitle = 'Mercedes-Benz Lease GAP Insurance';
      $rootScope.showCart=false;
}]);

angular.module('app').controller('productMinorDamageCtrl', ['$scope', '$http', '$rootScope', '$location', '$anchorScroll',
  function($scope, $http, $rootScope, $location, $anchorScroll) {
      $scope.pageTitle = 'Mercedes-Benz Minor Damage Insurance';
      $rootScope.showCart=false;
      $scope.dofocus=function(sectionName){
		$location.hash(sectionName);
		$anchorScroll();
	  }
}]);

angular.module('app').controller('productMbiCtrl', ['$scope', '$http', '$rootScope', '$location', '$anchorScroll',
  function($scope, $http, $rootScope, $location, $anchorScroll) {
      $scope.pageTitle = 'Mercedes-Benz Mechanical Breakdown Insurance';
      $rootScope.showCart=false;
      $scope.dofocus=function(sectionName){
    $location.hash(sectionName);
    $anchorScroll();
    }
}]);

angular.module('app').controller('productAlloyCtrl', ['$scope', '$http', '$rootScope', '$location', '$anchorScroll',
  function($scope, $http, $rootScope, $location, $anchorScroll) {
      $scope.pageTitle = 'Mercedes-Benz Alloy Wheel Insurance';
      $rootScope.showCart=false;
      $scope.dofocus=function(sectionName){
    $location.hash(sectionName);
    $anchorScroll();
    }
}]);

angular.module('app').controller('loginCtrl', ['$scope', '$http', '$rootScope', '$location', '$anchorScroll',
  function($scope, $http, $rootScope, $location, $anchorScroll) {
      $scope.pageTitle = 'Login';
      $rootScope.showCart=false;
      $scope.dofocus=function(sectionName){
    $location.hash(sectionName);
    $anchorScroll();
    }
}]);

angular.module('app').controller('upgradeCtrl', ['$scope', '$http', '$rootScope', '$location', '$anchorScroll',
  function($scope, $http, $rootScope, $location, $anchorScroll) {
      $scope.pageTitle = 'Upgrade';
      $rootScope.showCart=false;
      $scope.dofocus=function(sectionName){
    $location.hash(sectionName);
    $anchorScroll();
    }
}]);

angular.module('app').controller('404Ctrl', ['$scope', '$http', '$rootScope', '$location', '$anchorScroll',
  function($scope, $http, $rootScope, $location, $anchorScroll) {
      $scope.pageTitle = '404 Page not found';
      $rootScope.showCart=false;
      $scope.dofocus=function(sectionName){
    $location.hash(sectionName);
    $anchorScroll();
    }
}]);

angular.module('app').controller('500Ctrl', ['$scope', '$http', '$rootScope', '$location', '$anchorScroll',
  function($scope, $http, $rootScope, $location, $anchorScroll) {
      $scope.pageTitle = 'Internal Server Error';
      $rootScope.showCart=false;
      $scope.dofocus=function(sectionName){
    $location.hash(sectionName);
    $anchorScroll();
    }
}]);

angular.module('app').controller('cartLoginCtrl', ['$scope', '$http', '$rootScope', '$location',
  function($scope, $http, $rootScope, $location) {
      $scope.pageTitle = 'Register or Sign in';
      $rootScope.showCart=true;
}]);

angular.module('app').controller('cartVehicleCtrl', ['$scope', '$http', '$rootScope', '$location','$anchorScroll',
  function($scope, $http, $rootScope, $location, $anchorScroll) {
        $scope.pageTitle = 'About the Vehicle';
        $rootScope.showCart=true;
        $scope.dofocus=function(sectionName){
    $location.hash(sectionName);
    $anchorScroll();
    }
}]);

angular.module('app').controller('cartDriverCtrl', ['$scope', '$http', '$rootScope', '$location',
  function($scope, $http, $rootScope, $location) {
      $scope.pageTitle = 'About the Driver';
      $rootScope.showCart=true;
}]);

angular.module('app').controller('cartDemandsCtrl', ['$scope', '$http', '$rootScope', '$location',
  function($scope, $http, $rootScope, $location) {
      $scope.pageTitle = 'Your Product';
      $rootScope.showCart=true;
}]);

angular.module('app').controller('cartPaymentCtrl', ['$scope', '$http', '$rootScope', '$location',
  function($scope, $http, $rootScope, $location) {
      $scope.pageTitle = 'Payment Options';
      $rootScope.showCart=true;
}]);

angular.module('app').controller('cartPaymentErrorCtrl', ['$scope', '$http', '$rootScope', '$location',
  function($scope, $http, $rootScope, $location) {
      $scope.pageTitle = 'Payment Cancelled';
      $rootScope.showCart=false;
}]);

angular.module('app').controller('confirmCoverCtrl', ['$scope', '$http', '$rootScope', '$location',
  function($scope, $http, $rootScope, $location) {
      $scope.pageTitle = 'Confirmation of Cover';
      $rootScope.showCart=false;
}]);

angular.module('app').controller('policyCreationCtrl', ['$scope', '$http', '$rootScope', '$location',
  function($scope, $http, $rootScope, $location) {
      $scope.pageTitle = 'Your Policy is being created...';
      $rootScope.showCart=false;
}]);

angular.module('app').controller('pcInstallmentsCtrl', ['$scope', '$http', '$rootScope', '$location',
  function($scope, $http, $rootScope, $location) {
      $scope.pageTitle = 'Pay by Installments';
      $rootScope.showCart=false;
}]);

angular.module('app').controller('depositCtrl', ['$scope', '$http', '$rootScope', '$location',
  function($scope, $http, $rootScope, $location) {
      $scope.pageTitle = 'Deposit Payment';
      $rootScope.showCart=false;
}]);

angular.module('app').controller('installmentsCtrl', ['$scope', '$http', '$rootScope', '$location',
  function($scope, $http, $rootScope, $location) {
      $scope.pageTitle = 'Installment Set up';
      $rootScope.showCart=false;
}]);

angular.module('app').controller('worldpay1Ctrl', ['$scope', '$http', '$rootScope', '$location',
  function($scope, $http, $rootScope, $location) {
      $scope.pageTitle = 'Fake WorldPay Page - Payment Selection (Step 1)';
      $rootScope.showCart=false;
}]);

angular.module('app').controller('worldpay2Ctrl', ['$scope', '$http', '$rootScope', '$location',
  function($scope, $http, $rootScope, $location) {
      $scope.pageTitle = 'Fake WorldPay Page - Payment Details (Step 2)';
      $rootScope.showCart=false;
}]);

angular.module('app').controller('worldpay3Ctrl', ['$scope', '$http', '$rootScope', '$location',
  function($scope, $http, $rootScope, $location) {
      $scope.pageTitle = 'Fake WorldPay Page - Confirmation (Step 3)';
      $rootScope.showCart=false;
}]);


angular.module('app').controller('contactCtrl', ['$scope', '$http', '$rootScope', '$location',
  function($scope, $http, $rootScope, $location) {
      $scope.pageTitle = 'Contacts';
      $rootScope.showCart=false;
}]);

angular.module('app').controller('contactSupportCtrl', ['$scope', '$http', '$rootScope', '$location',
  function($scope, $http, $rootScope, $location) {
      $scope.pageTitle = 'Contact Technical Support';
      $rootScope.showCart=false;
}]);



angular.module('app').controller('cartCardpayCtrl', ['$scope', '$http', '$rootScope', '$location',
  function($scope, $http, $rootScope, $location) {
      $scope.pageTitle = 'Card Payment';
      $rootScope.showCart=true;
}]);


angular.module('app').controller('productsAllCtrl', ['$scope', '$http', '$rootScope', '$location',
  function($scope, $http, $rootScope, $location) {
      $scope.pageTitle = 'Our Products';
      $rootScope.showCart=false;
    $('.bxslider').bxSlider();
}]);

angular.module('app').controller('login-Error', ['$scope', '$http', '$rootScope', '$location',
  function($scope, $http, $rootScope, $location) {
      $scope.pageTitle = 'Login Error';
      $rootScope.showCart=false;
}]);


angular.module('app').controller('welcomeCtrl', ['$scope', '$http', '$rootScope', '$location','$anchorScroll',
  function($scope, $http, $rootScope, $location, $anchorScroll) {

        $scope.open2 = function() {
          $scope.popup2.opened = true;
        };
        $scope.popup2 = { opened: false };

        $scope.pageTitle = 'Welcome Mr Porter';
        $rootScope.showCart=false;
        $scope.doSomethingWithDate = function (date){
  alert(date);
};
        $scope.dofocus=function(sectionName){
        $location.hash(sectionName);
        $anchorScroll();
    }
}]);

angular.module('app').controller('quotesCtrl', ['$scope', '$http', '$rootScope', '$location','$anchorScroll',
  function($scope, $http, $rootScope, $location, $anchorScroll) {

        $scope.openCalendarForGAP = function() {
          $scope.gapCalendarOpened = true;
        };
        $scope.gapCalendarOpened = false

        $scope.openCalendarForAlloy = function() {
          $scope.alloyCalendarOpened = true;
        };
        $scope.alloyCalendarOpened = false



        $scope.disableAddToBasketGap = true;
        $scope.disableAddToBasketAlloy = true;

        $scope.pageTitle = 'Your Quote';
        $rootScope.showCart = true;

        $scope.$watch("dtGap", function (newValue, oldValue) {
            
            if (typeof newValue != "undefined")
            {
                //Added your script here to disable element
                $scope.disableAddToBasketGap = false;

            }
        })
      $scope.$watch("dtAlloy", function (newValue, oldValue) {
            
            if (typeof newValue != "undefined")
            {
                //Added your script here to disable element
                $scope.disableAddToBasketAlloy = false;
            }
        })

        $scope.doSomethingWithDate = function (date){
  alert(date);
};
        $scope.dofocus=function(sectionName){
        $location.hash(sectionName);
        $anchorScroll();
    }
}]);

angular.module('app').controller('quotescompleteCtrl', ['$scope', '$http', '$rootScope', '$location','$anchorScroll',
  function($scope, $http, $rootScope, $location, $anchorScroll) {

        $scope.openCalendarForGAP = function() {
          $scope.gapCalendarOpened = true;
        };
        $scope.gapCalendarOpened = false

        $scope.openCalendarForAlloy = function() {
          $scope.alloyCalendarOpened = true;
        };
        $scope.alloyCalendarOpened = false



        $scope.disableAddToBasketGap = true;
        $scope.disableAddToBasketAlloy = true;

        $scope.pageTitle = 'Your Quote';
        $rootScope.showCart = true;

        $scope.$watch("dtGap", function (newValue, oldValue) {
            
            if (typeof newValue != "undefined")
            {
                //Added your script here to disable element
                $scope.disableAddToBasketGap = false;

            }
        })
      $scope.$watch("dtAlloy", function (newValue, oldValue) {
            
            if (typeof newValue != "undefined")
            {
                //Added your script here to disable element
                $scope.disableAddToBasketAlloy = false;
            }
        })

        $scope.doSomethingWithDate = function (date){
  alert(date);
};
        $scope.dofocus=function(sectionName){
        $location.hash(sectionName);
        $anchorScroll();
    }
}]);

angular.module('app').controller('confirmCtrl', ['$scope', '$http', '$rootScope', '$location','$anchorScroll',
  function($scope, $http, $rootScope, $location, $anchorScroll) {
        $scope.pageTitle = 'Confirm Your Details';
        $rootScope.showCart=true;
        $scope.dofocus=function(sectionName){
    $location.hash(sectionName);
    $anchorScroll();
    }
}]);

angular.module('app').controller('mybasketCtrl', ['$scope', '$http', '$rootScope', '$location','$anchorScroll',
  function($scope, $http, $rootScope, $location, $anchorScroll) {
        $scope.pageTitle = 'Your Basket';
        $rootScope.showCart=true;
        $scope.dofocus=function(sectionName){
    $location.hash(sectionName);
    $anchorScroll();
    }
}]);

angular.module('app').controller('mybasketExpiredCtrl', ['$scope', '$http', '$rootScope', '$location','$anchorScroll',
  function($scope, $http, $rootScope, $location, $anchorScroll) {
        $scope.pageTitle = 'Your Basket [Sample Error]';
        $rootScope.showCart=true;
        $scope.dofocus=function(sectionName){
    $location.hash(sectionName);
    $anchorScroll();
    }
}]);

angular.module('app').controller('mybasketExpiredAltCtrl', ['$scope', '$http', '$rootScope', '$location','$anchorScroll',
  function($scope, $http, $rootScope, $location, $anchorScroll) {
        $scope.pageTitle = 'Your Basket [Alternative Sample Error]';
        $rootScope.showCart=true;
        $scope.dofocus=function(sectionName){
    $location.hash(sectionName);
    $anchorScroll();
    }
}]);

angular.module('app').controller('myaccountCtrl', ['$scope', '$http', '$rootScope', '$location','$anchorScroll',
  function($scope, $http, $rootScope, $location, $anchorScroll) {
        $scope.pageTitle = 'Your Account';
        $rootScope.showCart=false;
        $scope.dofocus=function(sectionName){
    $location.hash(sectionName);
    $anchorScroll();
    }
}]);

angular.module('app').controller('policyDocsCtrl', ['$scope', '$http', '$rootScope', '$location','$anchorScroll',
  function($scope, $http, $rootScope, $location, $anchorScroll) {
        $scope.pageTitle = 'Policy Documents';
        $rootScope.showCart=false;
        $scope.dofocus=function(sectionName){
    $location.hash(sectionName);
    $anchorScroll();
    }
}]);

angular.module('app').controller('quoteDocsCtrl', ['$scope', '$http', '$rootScope', '$location','$anchorScroll',
  function($scope, $http, $rootScope, $location, $anchorScroll) {
        $scope.pageTitle = 'Quote Documentation';
        $rootScope.showCart=false;
        $scope.dofocus=function(sectionName){
    $location.hash(sectionName);
    $anchorScroll();
    }
}]);

angular.module('app').controller('recallCtrl', ['$scope', '$http', '$rootScope', '$location','$anchorScroll',
  function($scope, $http, $rootScope, $location, $anchorScroll) {
        $scope.pageTitle = 'Recall your Quote';
        $rootScope.showCart=false;
        $scope.dofocus=function(sectionName){
    $location.hash(sectionName);
    $anchorScroll();
    }
}]);

angular.module('app').controller('providerCtrl', ['$scope', '$http', '$rootScope', '$location','$anchorScroll',
  function($scope, $http, $rootScope, $location, $anchorScroll) {
        $scope.pageTitle = 'Provider';
        $rootScope.showCart=false;
        $scope.dofocus=function(sectionName){
    $location.hash(sectionName);
    $anchorScroll();
    }
}]);

angular.module('app').controller('legalCtrl', ['$scope', '$http', '$rootScope', '$location','$anchorScroll',
  function($scope, $http, $rootScope, $location, $anchorScroll) {
        $scope.pageTitle = 'Legal Notice';
        $rootScope.showCart=false;
        $scope.dofocus=function(sectionName){
    $location.hash(sectionName);
    $anchorScroll();
    }
}]);

angular.module('app').controller('cookiesCtrl', ['$scope', '$http', '$rootScope', '$location','$anchorScroll',
  function($scope, $http, $rootScope, $location, $anchorScroll) {
        $scope.pageTitle = 'Cookies';
        $rootScope.showCart=false;
        $scope.dofocus=function(sectionName){
    $location.hash(sectionName);
    $anchorScroll();
    }
}]);

angular.module('app').controller('privacyCtrl', ['$scope', '$http', '$rootScope', '$location','$anchorScroll',
  function($scope, $http, $rootScope, $location, $anchorScroll) {
        $scope.pageTitle = 'Privacy Statement';
        $rootScope.showCart=false;
        $scope.dofocus=function(sectionName){
    $location.hash(sectionName);
    $anchorScroll();
    }
}]);

angular.module('app').controller('forgotCtrl', ['$scope', '$http', '$rootScope', '$location','$anchorScroll',
  function($scope, $http, $rootScope, $location, $anchorScroll) {
        $scope.pageTitle = 'Forgot Password?';
        $rootScope.showCart=false;
        $scope.dofocus=function(sectionName){
    $location.hash(sectionName);
    $anchorScroll();
    }
}]);

angular.module('app').controller('resetsentCtrl', ['$scope', '$http', '$rootScope', '$location','$anchorScroll',
  function($scope, $http, $rootScope, $location, $anchorScroll) {
        $scope.pageTitle = 'Password reset sent!';
        $rootScope.showCart=false;
        $scope.dofocus=function(sectionName){
    $location.hash(sectionName);
    $anchorScroll();
    }
}]);

angular.module('app').controller('expiredCtrl', ['$scope', '$http', '$rootScope', '$location','$anchorScroll',
  function($scope, $http, $rootScope, $location, $anchorScroll) {
        $scope.pageTitle = 'Password Reset Expired';
        $rootScope.showCart=false;
        $scope.dofocus=function(sectionName){
    $location.hash(sectionName);
    $anchorScroll();
    }
}]);

angular.module('app').controller('newpasswordCtrl', ['$scope', '$http', '$rootScope', '$location','$anchorScroll',
  function($scope, $http, $rootScope, $location, $anchorScroll) {
        $scope.pageTitle = 'Reset your Password';
        $rootScope.showCart=false;
        $scope.dofocus=function(sectionName){
    $location.hash(sectionName);
    $anchorScroll();
    }
}]);

angular.module('app').controller('styleCtrl', ['$scope', '$http', '$rootScope', '$location','$anchorScroll',
  function($scope, $http, $rootScope, $location, $anchorScroll) {

        $scope.openCalendarForGAP = function() {
          $scope.gapCalendarOpened = true;
        };
        $scope.gapCalendarOpened = false

        $scope.openCalendarForAlloy = function() {
          $scope.alloyCalendarOpened = true;
        };
        $scope.alloyCalendarOpened = false



        $scope.disableAddToBasketGap = true;
        $scope.disableAddToBasketAlloy = true;

        $scope.pageTitle = 'Style Guide';
        $rootScope.showCart = true;

        $scope.$watch("dtGap", function (newValue, oldValue) {
            
            if (typeof newValue != "undefined")
            {
                //Added your script here to disable element
                $scope.disableAddToBasketGap = false;

            }
        })
      $scope.$watch("dtAlloy", function (newValue, oldValue) {
            
            if (typeof newValue != "undefined")
            {
                //Added your script here to disable element
                $scope.disableAddToBasketAlloy = false;
            }
        })

        $scope.doSomethingWithDate = function (date){
  alert(date);
};
        $scope.dofocus=function(sectionName){
        $location.hash(sectionName);
        $anchorScroll();
    }
}]);



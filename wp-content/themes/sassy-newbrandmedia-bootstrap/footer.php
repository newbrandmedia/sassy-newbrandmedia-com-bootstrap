			<div class="container">
				<footer role="contentinfo">
				
					<div id="inner-footer" class="clearfix">
			          <hr />
			          <div id="widget-footer" class="clearfix row">
			            <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('footer1') ) : ?>
			            <?php endif; ?>
			            <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('footer2') ) : ?>
			            <?php endif; ?>
			            <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('footer3') ) : ?>
			            <?php endif; ?>
			          </div>
						

					</div> <!-- end #inner-footer -->
					
				</footer> <!-- end footer -->
			</div>
		</div> <!-- end #container -->
		<div class="row">
			<div class="brandComp-darkest footer">
				<div class="container">
				 <div class="row">
					<div class="col-sm-6">	
						<nav class="clearfix">
							<?php wp_bootstrap_footer_links(); // Adjust using Menus in Wordpress Admin ?>
						</nav>
						<p class="attribution">&copy; <?php bloginfo('name'); ?></p>
					</div>	
					<div class="col-sm-6">	
						<p class="attribution-supplier"><a href="http://www.newbrandmedia.com" title="Digital by New Brand Media">Digital by New Brand Media</a></p>
				
						
					</div>
				 </div>	
				</div>	
			</div>
		</div>
</div> <!-- END landing Page row -->				
		<!--[if lt IE 7 ]>
  			<script src="//ajax.googleapis.com/ajax/libs/chrome-frame/1.0.3/CFInstall.min.js"></script>
  			<script>window.attachEvent('onload',function(){CFInstall.check({mode:'overlay'})})</script>
		<![endif]-->
		
		<?php wp_footer(); // js scripts are inserted using this function ?>

	</body>

</html>